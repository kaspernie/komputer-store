# Komputer Store

This is the first assignment in the Noroff's JavaScript module. See [./Komputer_Store_App.pdf](./Komputer_Store_App.pdf) for details.

## Install
Clone or download repository

## Usage
View public/index.html in a modern web browser by either
- serving the file on a remote webserver
- serving the file on a local webserver
- open the file directly from a local drive

## Maintainers

Kasper Nielsen
[https://gitlab.com/kaspernie]
