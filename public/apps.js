// ELEMENTS FOR BANK AND LOAN
const bankBalanceElement = document.getElementById("bankBalance");
const loanBalanceElement = document.getElementById("loanBalance");
const loanBalanceAreaElement = document.getElementById("loanBalanceArea");
const getLoanButton = document.getElementById("getLoanButton");

// ELEMENTS FOR WORK
const payBalanceElement = document.getElementById("payBalance");
const workButton = document.getElementById("workButton");
const repayLoanButton = document.getElementById("repayLoanButton");

// ELEMENTS FOR LAPTOP AND BUY
const selectLaptopsElement = document.getElementById("selectLaptops");
const laptopSpecsElement = document.getElementById("laptopSpecs");
const laptopInfoAreaElement = document.getElementById("laptopInfoArea");
const laptopImgElement = document.getElementById("laptopImg");
const laptopDescElement = document.getElementById("laptopDesc");
const laptopBuyElement = document.getElementById("laptopBuy");
//

//
// BANK BALANCE AND LOAN
//
let bankBalance = 200;
let loanBalance = 0;
let loanRequest = 10;

loanBalanceElement.innerText = loanBalance;

// Function that will display or hide "Repay Loan" button and loan balance depending on loan existance
const displayOrHideLoan = () => {
  let vis = "hidden";
  if (loanBalance > 0) {
    vis = "visible";
  }
  repayLoanButton.style.visibility = vis;
  loanBalanceAreaElement.style.visibility = vis;
};
displayOrHideLoan();

// When "Get Loan" is clicked
getLoanButton.addEventListener("click", function () {
  if (loanBalance) {
    alert("Sorry, you have to repay your old loan before getting a new loan");
  } else {
    loanRequest = Number(
      prompt("Please, enter the size of the desired loan:", 0)
    );

    if (Number.isInteger(loanRequest) && loanRequest > 0) {
      const creditLimit = 2 * bankBalance;
      if (loanRequest <= creditLimit) {
        alert(`Your loan request of ${loanRequest} DKK was granted.`);
        loanBalance = loanRequest;
        loanBalanceElement.innerText = loanBalance;
        bankBalance += loanRequest;
        bankBalanceElement.innerText = bankBalance;
      } else {
        alert(`Sorry, your credit limit is ${creditLimit} DKK.`);
      }
    } else {
      alert("Sorry, the requested amount must enter a positive integer");
    }
  }
  displayOrHideLoan();
});

//
// WORK, BANK AND REPAY
//

let payBalance = 0;

const handleWork = () => {
  payBalance = payBalance + 100;
  payBalanceElement.innerText = payBalance;
};
workButton.addEventListener("click", handleWork);

const handleBank = () => {
  // First, deduct up to 10% of pay balance to repay loan
  if (loanBalance) {
    let partOfPayToRepay = 0.1 * payBalance;
    if (partOfPayToRepay > loanBalance) {
      partOfPayToRepay = loanBalance;
    }
    loanBalance -= partOfPayToRepay;
    loanBalanceElement.innerText = loanBalance;
    payBalance -= partOfPayToRepay;
  }
  // Remaining pay balance is transferred to bank balance
  bankBalance = bankBalance + payBalance;
  bankBalanceElement.innerText = bankBalance;
  payBalance = 0; // Pay balance is reset
  payBalanceElement.innerText = payBalance;
  displayOrHideLoan();
};
bankButton.addEventListener("click", handleBank);

const handleRepayLoan = () => {
  let partOfPayToRepay = payBalance;
  let partOfPayToBank = 0;
  if (payBalance > loanBalance) {
    partOfPayToRepay = loanBalance;
    partOfPayToBank = payBalance - loanBalance;
    // Remaining pay balance is transferred to bank balance
    bankBalance += partOfPayToBank;
    bankBalanceElement.innerText = bankBalance;
  }
  // Repay loan
  loanBalance -= partOfPayToRepay;
  loanBalanceElement.innerText = loanBalance;
  payBalance = 0; // Pay balance is reset
  payBalanceElement.innerText = payBalance;
  displayOrHideLoan();
};
repayLoanButton.addEventListener("click", handleRepayLoan);

//
// LAPTOP AND BUY
//

// Fetch laptop data and populate menu
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
  .then((response) => response.json())
  .then((laptops_fetched) => (laptops = laptops_fetched))
  .then((laptops) => addLaptopsToMenu(laptops));

const addLaptopsToMenu = (laptops) => {
  // Prepend instructions to laptop array
  const noLaptop = {
    id: 0,
    title: "-- Please, select a laptop --",
    active: false, // used later for *not* displaying specs+info
  };
  laptops.unshift(noLaptop);
  laptops.forEach((x) => addLaptopToMenu(x));
};

const addLaptopToMenu = (laptop) => {
  const laptopElement = document.createElement("option");
  laptopElement.value = laptop.id;
  laptopElement.appendChild(document.createTextNode(laptop.title));
  selectLaptopsElement.appendChild(laptopElement);
};

const handleLaptopMenuChange = (e) => {
  const selectedLaptop = laptops[e.target.selectedIndex];

  // If an active laptop is selected,
  //  then show laptop specs and info,
  //  otherwise empty area
  if (selectedLaptop.active) {
    // Show specs for selected laptop
    laptopSpecsElement.innerHTML = "<br/><b>Features:</b>";
    let ul = document.createElement("ul");
    selectedLaptop.specs.forEach((item) => {
      let li = document.createElement("li");
      li.innerText = item;
      ul.appendChild(li);
    });
    laptopSpecsElement.append(ul);

    laptopInfoAreaElement.style.visibility = "visible";

    // Show laptop image
    laptopImgElement.src = selectedLaptop.image;

    // Show laptop description
    laptopDescElement.innerHTML = `<h2>${selectedLaptop.title}</h2>`;
    laptopDescElement.innerHTML += selectedLaptop.description;

    // Show price and buy button for selected laptop

    laptopBuyElement.innerHTML = `<h3>${selectedLaptop.price} DKK</h3>`;

    // Create the button and append to element
    const buyButton = document.createElement("button");
    buyButton.innerHTML = "BUY NOW";
    // buyButton.style.cssFloat = "right";
    // // const foo = document.createElement("div")
    // // foo.innerHTML += `<h3>${selectedLaptop.price} DKK</h3>`;
    laptopBuyElement.appendChild(buyButton);
    laptopInfoAreaElement.style.visibility = "visible";
    // When "BUY" is clicked
    buyButton.addEventListener("click", function () {
      if (bankBalance >= selectedLaptop.price) {
        alert(
          `Thank you for your purchase. You are now the owner of the laptop "${selectedLaptop.title}"`
        );
        bankBalance = bankBalance - selectedLaptop.price;
        bankBalanceElement.innerText = bankBalance;
      } else {
        alert("Sorry, you don't have sufficient funds...");
      }
    });
  } else {
    laptopSpecsElement.innerHTML = "";
    laptopInfoAreaElement.style.visibility = "hidden";
  }
};

selectLaptopsElement.addEventListener("change", handleLaptopMenuChange);
